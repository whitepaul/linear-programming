//
// Created by paul on 10.05.20.
//

#include "PrimerSimplexAlgorithm.h"
#include <memory>
PrimerSimplexAlgorithm::PrimerSimplexAlgorithm(LinearProblem& lp){
    LP = std::make_unique<LinearProblem>(lp);
}

PrimerSimplexAlgorithm::~PrimerSimplexAlgorithm() {
    LP.release();
};

