#include <iostream>

#include "LinearProblem.h"
#include "PrimerSimplexAlgorithm.h"

int main() {
    const char* filename = "input.txt";
    try{
        LinearProblem LP(filename);
        std::cout << LP << std::endl;

        PrimerSimplexAlgorithm PSA(LP);
    }catch(...){
        std::cout << "LinearProblem wasn't initialised." << std::endl;
    }
    return 0;
}

