cmake_minimum_required(VERSION 3.15)
project(simplex)

set(CMAKE_CXX_STANDARD 17)

add_executable(main main.cpp )
add_library(simplex_algorithm LinearProblem.cpp LinearProblem.h PrimerSimplexAlgorithm.cpp PrimerSimplexAlgorithm.h)
target_link_libraries(main simplex_algorithm)
target_include_directories(main PRIVATE /home/paul/boost_1_72_0/boost/numeric)

