//
// Created by paul on 10.05.20.
//

#ifndef SIMPLEX_PRIMERSIMPLEXALGORITHM_H
#define SIMPLEX_PRIMERSIMPLEXALGORITHM_H

#include <cstddef>
#include <vector>
#include <memory>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include "LinearProblem.h"


class PrimerSimplexAlgorithm{
    std::unique_ptr<LinearProblem> LP = nullptr;
    ublas::matrix<double> B;
    ublas::vector<double> theta;
    ublas::vector<double> reduce_costs;

    ublas::vector<double> beta;
    //simplex multiplier c_b * B^-1
    ublas::vector<double> pi;
    ublas::vector<double> alpha_i;

    //it should do the following:
    //1. Found initial feasible basis variables
    //2. Compute B^-1
    //3. Compute beta
/*    void init();

    void chooseRow(std::size_t column);
    void updateMatrix(std::size_t column, std::size_t row);

    [[nodiscard]] bool IsUnbounded() const noexcept;
    [[nodiscard]] bool IsOptimal() const noexcept;
*/
public:
    explicit PrimerSimplexAlgorithm(LinearProblem&);
    ~PrimerSimplexAlgorithm();
};


#endif //SIMPLEX_PRIMERSIMPLEXALGORITHM_H
