//
// Created by paul on 04.05.20.
//

#ifndef SIMPLEX_LINEARPROBLEM_H
#define SIMPLEX_LINEARPROBLEM_H

#include <cstddef>
#include <vector>
#include <ostream>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>




using namespace boost::numeric;

// describes Linear Problem (LP) in form:
// - minimizing objective function
// - constraints in the form of equalities
class LinearProblem{
    //M - rows, N - columns
    std::size_t M, N;
    ublas::vector<double> c;
    ublas::vector<double> b;
    ublas::matrix<double> A;

    std::vector<std::size_t> basic_vars;
    bool optimal = false;
    double optimalValue;
public:
/*
    LinearProblem(std::size_t M, std::size_t N, ublas::vector<double> c,
                                                 ublas::vector<double> b,
                                                 ublas::matrix<double> A,
                                                 std::vector<std::size_t>& basic_vars);
*/

    explicit LinearProblem(const char* fname);
    LinearProblem(const LinearProblem&) = default;
    LinearProblem(LinearProblem&&) = default;
    LinearProblem& operator=(const LinearProblem&) = default;
    LinearProblem& operator=(LinearProblem&&) = default;
    ~LinearProblem() = default;

    friend std::ostream& operator<<(std::ostream& os, const LinearProblem& LP);

    [[nodiscard]] std::size_t getRowDim() const noexcept;
    [[nodiscard]] std::size_t getColDim() const noexcept;
    [[nodiscard]] bool isOptimal() const noexcept;
    [[nodiscard]] double getOptimalValue() const;
};

#endif //SIMPLEX_LINEARPROBLEM_H
