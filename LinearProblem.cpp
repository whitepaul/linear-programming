//
// Created by paul on 04.05.20.
//

#include "LinearProblem.h"

#include <fstream>
#include <iostream>
#include <stdexcept>

LinearProblem::LinearProblem(const char *fname) {
    std::ifstream ifs(fname);
    if (!ifs.is_open()) {
        std::cout << "Error: file " << fname << " wasn't opened." << std::endl;
        throw std::exception();
    }

    ifs >> N >> M;

    c.resize(N);
    for (unsigned i = 0; i < N; ++i) {
        ifs >> c(i);
    }



    b.resize(M);
    for (unsigned i = 0; i < M; ++i) {
        ifs >> b(i);
    }


    A.resize(M, N);
    for (unsigned i = 0; i < M; ++i) {
        for (unsigned j = 0; j < N; ++j) {
            ifs >> A(i, j);
        }
    }


    if (!ifs) {
        std::cout << "Error: in file " << fname << " while reading." << std::endl;
        throw std::exception();
    }

    ifs.close();
}

std::size_t LinearProblem::getRowDim() const noexcept {
    return M;
}

std::size_t LinearProblem::getColDim() const noexcept {
    return N;
}

bool LinearProblem::isOptimal() const noexcept {
    return optimal;
}

double LinearProblem::getOptimalValue() const {
    if (!isOptimal()) {
        throw std::logic_error("Optimal solution of LP wasn't obtained yet.");
    }
    return optimalValue;
}

std::ostream& operator<<(std::ostream& os, const LinearProblem& LP){
    os << "c = " << LP.c << std::endl;
    os << "A = " << std::endl << LP.A << std::endl;
    os << "b = " << LP.b << std::endl;

    os << "Optimal solution: " ;
    if (LP.optimal){
        os <<  LP.optimalValue << std::endl;
    }
    else {
        os << "not obtained yet";
    }

    return os;
}

/*
LinearProblem::LinearProblem(const LinearProblem &other){
    *this = other;
}

LinearProblem& LinearProblem::operator=(const LinearProblem &other) {
    M = other.getRowDim();
    N = other.getColDim();
    c = other.c;
    A = other.A;
    b = other.b;
    basic_vars = other.basic_vars;
    optimal = other.optimal;
    optimalValue = other.optimalValue;
    return *this;
}
*/


